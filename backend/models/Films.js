// External Dependancies
const mongoose = require('mongoose')
// const ObjectId = mongoose.Schema.Types.ObjectId

const FilmSchema = new mongoose.Schema({
	slug: String,
	title: String,
	money: String,
	year: Number,
	director: String,
	author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }

})



module.exports = mongoose.model('Film', FilmSchema)