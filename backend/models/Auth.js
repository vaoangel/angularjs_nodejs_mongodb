const mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var secret = require('../config').secret;
var crypto = require('crypto');

const AuthSchema = new mongoose.Schema({
    username : String,
    email: String,
    image: String,
    hash: String,
    favorites: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Films' }],
    following: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    descr: String,
    salt: String
})
AuthSchema.methods.setPassword = function(password){
  // console.log("entra");
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

AuthSchema.methods.toAuthJSON = function(){
  return {
    username: this.username,
    email: this.email,
    token: this.generateJWT(),
    image: this.image
  };
};
AuthSchema.methods.validPassword = function(password,salty) {
  
  var hash = crypto.pbkdf2Sync(password, salty, 10000, 512, 'sha512').toString('hex');
  // console.log(hash);
  return  hash;
};

AuthSchema.methods.generateJWT = function() {
  var today = new Date();
  var exp = new Date(today);
  exp.setDate(today.getDate() + 60);

  return jwt.sign({
    id: this._id,
    username: this.username,
    exp: parseInt(exp.getTime() / 1000),
  }, secret);
};
AuthSchema.methods.favorite = function(id){
  if(this.favorites.indexOf(id) === -1){
    this.favorites.push(id);
  }

  return this.save();
};

AuthSchema.methods.follow = function(id){
  if(this.following.indexOf(id) === -1){
    this.following.push(id);
  }

  return this.save();
};

AuthSchema.methods.unfollow = function(id){
  this.following.remove(id);
  return this.save();
};

AuthSchema.methods.isFollowing = function(id){
  return this.following.some(function(followId){
    return followId.toString() === id.toString();
  });
};
  module.exports = mongoose.model('Auth', AuthSchema);