var mongoose = require('mongoose');
var slug = require('slug');
var User = mongoose.model('User');

var RestaurantSchema = new mongoose.Schema({
    slug: {type: String, lowercase: true, unique: true},
    name: String,
    rate: Number,
    location: String,
    description: String,
    author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },


},{timestamps: true});


RestaurantSchema.pre('validate', function(next){
    if(!this.slug)  {
        this.slugify();
      }
    
      next();
})

RestaurantSchema.methods.slugify = function() {
    this.slug = slug(this.name);
  };


  RestaurantSchema.methods.toJSONFor = function(user){
    return {
      slug: this.slug,
      name: this.name,
      rate: this.rate,
      location: this.location,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      description: this.description,
      author: this.author.toProfileJSONFor(user)
    };
  };

  mongoose.model('Restaurants', RestaurantSchema);