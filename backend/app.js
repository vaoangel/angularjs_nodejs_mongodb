// Import Server
const fastify = require('./server.js')
// const fastify2 = require('fastify');
// const app = fastify2();
// const passport = require('passport');

// require('./config/passport');
// // Import external dependancies

// //Passport / fastify session
// const fastifySession = require('fastify-session');
// const fastifyCookie = require('fastify-cookie');

// app.use(fastifySession,{ secret: 'random', fastifyCookie: { maxAge: 60000 }, resave: false, saveUninitialized: false  });
// app.use(passport.initialize());
// app.use(passport.session());







const gql = require('fastify-gql')
// Import GraphQL Schema
const schema = require('./schema')
// console.log(schema);
var cors = require('cors');
fastify.use(cors());

// Register Fastify GraphQL
fastify.register(gql, {
	schema,
	graphiql: true
})

// Import Routes
const routes = require('./routes')
// console.log(gql);
// Import Swagger Options
const swagger = require('./config/swagger')

// Register Swagger
fastify.register(require('fastify-swagger'), swagger.options)

// Loop over each route
routes.forEach((route, index) => {
	fastify.route(route)
})

// Run the server!
const start = async () => {
	try {
		await fastify.listen(3050, '0.0.0.0')
		fastify.swagger()
		fastify.log.info(`server listening on ${fastify.server.address().port}`)
	} catch (err) {
		fastify.log.error(err)
		process.exit(1)
	}
}
start()