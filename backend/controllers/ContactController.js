var email = require('../utils/email.js');
const boom = require('boom')


exports.SendEmail = async req => {
    try{
        console.log(req);
        return email.sendEmail(req);
        
    }catch (err) {
        throw boom.boomify(err)
    }
}