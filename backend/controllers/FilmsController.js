// External Dependancies
const boom = require('boom')
// console.log("entra film controllers");
// Get Data Models
const Film = require('../models/Films')
const jwt = require('jsonwebtoken');
const auth = require('../models/Auth');

const secret = require('../config').secret;
const ValidateToken = require('../config/auth');
// Get all cars
exports.getFilms = async () => {
	console.log("entra getfilms")
	try {
		const film = await Film.find()
		return film
	} catch (err) {
		throw boom.boomify(err)
	}
}


exports.getBySlug = async  req => {
	// console.log("entra getfilms")
	try {
		// const slug = req.params === undefined ? req.slug : req.params.slug
		// console.log(req.params);
		const film = await Film.find({slug: req.params.slug})
		// console.log(req);
		return film
	} catch (err) {
		throw boom.boomify(err)
	}
}
exports.getById = async  req => {
	// console.log("entra getfilms")
	try {
		const id = req.params === undefined ? req.id : req.params.id
		const film = await Film.findById(id)
		// console.log(req);
		return film
	} catch (err) {
		throw boom.boomify(err)
	}
}

exports.myFilms = async req=> {
	console.log(req.body);
	var token = new ValidateToken();
	var pp = token.getTokenFromHeader(req);  
	jwt.verify(pp, secret, function(err, decoded) {
	//  console.log(decoded) // bar
	 if(!decoded){
		 return false;
	 }
	})

try{
	const user = await auth.findOne({email: req.body.dataForm})
	// console.log(user._id)
	const film = Film.find({author: user._id})
		return film
}catch (err) {
		throw boom.boomify(err)
	}
	
}