const boom = require('boom');

const Film = require('../models/Films');
const auth = require('../models/Auth');

const jwt = require('jsonwebtoken');
const secret = require('../config').secret;
const ValidateToken = require('../config/auth');

exports.addFilm = async req => {
    //  console.log(req.body.dataForm);
    var token = new ValidateToken();
   var pp = token.getTokenFromHeader(req);  
   jwt.verify(pp, secret, function(err, decoded) {
    // console.log(decoded) // bar
    if(!decoded){
        return false;
    }
   });
   try{
    const  user = await auth.findOne({email: req.body.dataForm.user.email})

    const film = new Film();

    film.slug = req.body.dataForm.filmSlug;
    film.title = req.body.dataForm.filmTitle;
    film.money = req.body.dataForm.money;
    film.year = req.body.dataForm.year;
    film.director = req.body.dataForm.director;
    film.author = user._id;

    const newFilm = await film.save();
   return newFilm
   }catch (err){
    throw boom.boomify(err)

   }
   
}