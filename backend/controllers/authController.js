var mongoose = require('mongoose');

var passport = require('passport');

const boom = require('boom');

const auth = require('../models/Auth');

const jwt = require('jsonwebtoken');
const secret = require('../config').secret;
const ValidateToken = require('../config/auth');

exports.register = async req =>{
    

try{
    const register = new auth();
    register.username = req.body.dataForm.username;
    register.email = req.body.dataForm.email;
    register.image = "default";
    register.descr = "default";
    register.setPassword(req.body.dataForm.password);
    console.log(register);
    const newregister = await register.save();
    return newregister;
}catch (err) {
    throw boom.boomify(err)
//console.log(hash2);
}
}

exports.login = async req =>{
    const login = new auth();
    var res = null;
    login.email= req.body.dataForm.email;
    login.password = req.body.dataForm.password;
    // login.salt = req.body.dataForm.salt;
    // console.log(login.salt)
     const my_auth = await auth.findOne({email: login.email})
     
    //  var pp = login.validPassword(login.password, my_auth.salt);
    //  return pp
     var pp = login.validPassword(login.password, my_auth.salt)
    //  console.log(my_auth)
    //  console.log(pp)
     if(pp != my_auth.hash){

         return "no va";
     }else{
        return res = ({my_auth: my_auth.toAuthJSON()});

     }

   
    

}

exports.updateUser = async req =>{
//   console.log(req.body)
  var token = new ValidateToken();
   var pp = token.getTokenFromHeader(req);  
   jwt.verify(pp, secret, function(err, decoded) {
    // console.log(decoded) // bar
    if(!decoded){
        return false;
    }
   });
   try{


    const user = new auth();

    user.username = req.body.user.username;
    user.email = req.body.user.email;
    user.image = req.body.user.image;
    user.setPassword(req.body.user.password);
    user.bio = req.body.user.bio;
    const userUpdate = await user.save();
    return userUpdate;
}catch(err){
    throw boom.boomify(err)
    //console.log(hash2);
}
   
}
exports.findFav = async req =>{
     console.log(req.body);
   
     var token = new ValidateToken();
     var pp = token.getTokenFromHeader(req); 
    //  console.log(pp) 
     jwt.verify(pp, secret, function(err, decoded) {
    //   console.log(decoded.id) // bar
      if(!decoded){
        return false;
      }else{
          
      }
       
     }); 

     const my_auth = await auth.findOne({email: req.body.user.email})
    //  console.log(my_auth);
    return my_auth;

    

  
        }

exports.favUser = async req =>{
    var token = new ValidateToken();
    var pp = token.getTokenFromHeader(req);  
    jwt.verify(pp, secret, function(err, decoded) {
    //  console.log(req.body+"body") // bar
     if(!decoded){
         return false;
     }else{
        auth.findById(decoded.id).then(function(user){
     
     return user.favorite(req.body.film);

        })
     }
    });       
}

exports.author = async req =>{
//   console.log(req.body)
    const my_auth = await auth.findById(req.body.user)
    // console.log(my_auth);
    return my_auth
}

exports.unfollow = async req => {
    // console.log(req.body.users.current)
    var token = new ValidateToken();
    var pp = token.getTokenFromHeader(req);  
    jwt.verify(pp, secret, function(err, decoded) {
    //  console.log(req.body+"body") // bar
     if(!decoded){
         return false;
     }
    })
     const my_auth = await auth.findOne({username: req.body.users.current})
// console.log(my_auth._id)
      auth.findById(my_auth.id).then(function(user){
     
         return user.unfollow(req.body.users.followed);
   
            })
    
}


exports.follow = async req => {
    // console.log(req.body.users.current)
    var token = new ValidateToken();
    var pp = token.getTokenFromHeader(req);  
    jwt.verify(pp, secret, function(err, decoded) {
    //  console.log(req.body+"body") // bar
     if(!decoded){
         return false;
     }
    })
     const my_auth = await auth.findOne({username: req.body.users.current})
// console.log(my_auth._id)
      auth.findById(my_auth.id).then(function(user){
     
         return user.follow(req.body.users.followed);
   
            })

    
}

exports.findAuthorById = async req => {
    // console.log(req.body)
    var token = new ValidateToken();
    var pp = token.getTokenFromHeader(req);  
    jwt.verify(pp, secret, function(err, decoded) {
    //  console.log(req.body+"body") // bar
     if(!decoded){
         return false;
     }
    })
    const my_auth = await auth.findById(req.body.author)
    // console.log(my_auth);
   return my_auth
   ;}

