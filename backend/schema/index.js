// Import External Dependancies
const graphql = require('graphql')
console.log("indexj schema");
// Destructure GraphQL functions
const {
	GraphQLSchema,
	GraphQLObjectType,
	GraphQLString,
	GraphQLInt,
	GraphQLID,
	GraphQLList,
	GraphQLNonNull
} = graphql

// Import Controllers
const FilmsController = require('../controllers/FilmsController');


// console.log("entra index schema");
const FilmType = new GraphQLObjectType({
	
	name: 'Film',
	fields: () => ({
		slug: { type: GraphQLString },
		title: { type: GraphQLString }, 
		money: { type: GraphQLString },
		year: { type: GraphQLString },
		director: { type: GraphQLString }
	
		// cars: {
		// 	type: new GraphQLList(carType),
		// 	async resolve(parent, args) {
		// 		return await ownerController.getOwnersCars({ id: parent._id })
		// 	}
		// }
	})
})

// console.log("dsasadsadasdads"+FilmType);

// const serviceType = new GraphQLObjectType({
// 	name: 'Service',
// 	fields: () => ({
// 		_id: { type: GraphQLID },
// 		car_id: { type: GraphQLID },
// 		name: { type: GraphQLString },
// 		date: { type: GraphQLString },
// 		car: {
// 			type: carType,
// 			async resolve(parent, args) {
// 				return await carController.getSingleCar({ id: parent.car_id })
// 			}
// 		}
// 	})
// })

// // Define Root Query
const RootQuery = new GraphQLObjectType({
	
	 name: 'RootQueryType',
	 fields : {
	 films: {
		
		type: new GraphQLList(FilmType),
		async resolve(parent, args) {
			
			return await FilmsController.getFilms()
		}
	},
	getBySlug:{
		 
		  type: FilmType,
		  args: { slug: { type: GraphQLString } },
		  async resolve(parent, args) {
			
			return await FilmsController.getBySlug(args)
		}
	  },

	  getById: {
		
		 
			type: FilmType,
			args: { id: { type: GraphQLID } },
			async resolve(parent, args) {
			  console.log("Arguments: "+args);
			  return await FilmsController.getById(args)
		  }
		
	  }
 	}
})

// // Define Mutations
// const Mutations = new GraphQLObjectType({
// 	name: 'Mutations',
// 	fields: {
// 		addCar: {
// 			type: carType,
// 			args: {
// 				title: { type: new GraphQLNonNull(GraphQLString) },
// 				brand: { type: new GraphQLNonNull(GraphQLString) },
// 				price: { type: GraphQLString },
// 				age: { type: GraphQLInt },
// 				owner_id: { type: GraphQLID }
// 			},
// 			async resolve(parent, args) {
// 				const data = await carController.addCar(args)
// 				return data
// 			}
// 		},
// 		editCar: {
// 			type: carType,
// 			args: {
// 				id: { type: new GraphQLNonNull(GraphQLID) },
// 				title: { type: new GraphQLNonNull(GraphQLString) },
// 				brand: { type: new GraphQLNonNull(GraphQLString) },
// 				price: { type: new GraphQLNonNull(GraphQLString) },
// 				age: { type: new GraphQLNonNull(GraphQLInt) },
// 				owner_id: { type: GraphQLID }
// 			},
// 			async resolve(parent, args) {
// 				const data = await carController.updateCar(args)
// 				return data
// 			}
// 		},
// 		deleteCar: {
// 			type: carType,
// 			args: {
// 				id: { type: new GraphQLNonNull(GraphQLID) }
// 			},
// 			async resolve(parent, args) {
// 				const data = await carController.deleteCar(args)
// 				return data
// 			}
// 		}
// 	}
// })

// Export the schema
module.exports = new GraphQLSchema({
	 query: RootQuery,
	// mutation: Mutations
	

})
