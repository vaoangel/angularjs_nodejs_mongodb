// Import our Controllers
const FilmsController = require('../controllers/FilmsController');
const ContactController = require('../controllers/ContactController');
const authController = require('../controllers/authController');
const edditorController = require('../controllers/EditorController');
const moleculerController = require('../controllers/MoleculerController');
const graphql = require('../schema/index');
// Import Swagger documentation
// const documentation = require('./documentation/carApi')

const routes = [
  {
    method: 'GET',
    url: '/api/films',
    handler: FilmsController.getFilms
  },
  {
    method: 'GET',
    url: '/api/getBySlug/:slug',
    handler: FilmsController.getBySlug
  },
  {
    method: 'GET',
    url: '/api/getById/:id',
    handler: FilmsController.getById
  },
  {
    method: 'POST',
    url: '/api/contact',
    handler: ContactController.SendEmail
  },
  {
    method: 'POST',
    url: '/api/register',
    handler: authController.register
  },
  {
    method: 'POST',
    url: '/api/login',
    handler: authController.login
  },
  {
    method: 'PUT',
    url: '/api/user',
    handler: authController.updateUser
  },
  {
    method: 'PUT',
    url: '/api/favorite',
    handler: authController.favUser
  },
  {
    method: 'PUT',
    url: '/api/findFav',
    handler : authController.findFav
  },
  {
    method: 'POST',
    url: '/api/addFilm',
    handler : edditorController.addFilm
  },
  {
    method: 'POST',
    url: '/api/myFilms',
    handler : FilmsController.myFilms
  },
  {
    method: 'POST',
    url: '/api/author',
    handler : authController.author
  },
  {
    method: 'DELETE',
    url: '/api/profiles/unfollow',
    handler : authController.unfollow
  },
  {
    method: 'POST',
    url : '/api/profiles/follow',
    handler : authController.follow
  },
  {
    method : 'POST',
    url: '/api/findAuthorById',
    handler : authController.findAuthorById
  },
  {
    method : 'GET',
    url : '/api/filmas',
    handler : moleculerController.findFilms
  }

]

module.exports = routes
