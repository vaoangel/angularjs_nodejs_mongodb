// Import external dependancies
const faker = require('faker')
const boom = require('boom')
// Import internal dependancies
const fastify = require('../server.js')
console.log("seed")

// Fake data
const Films = [
	{
		name: 'Netflix',
		models: ['S', 'E', 'X', 'Y']
	},
	{
		name: 'HBO',
		models: ['GLA', 'GLC', 'GLE', 'GLS']
	},
	{
		name: 'MEGADEDE',
		models: ['X4', 'Z3', 'M2', '7']
	},
	{
		name: 'PLUSDEDE',
		models: ['A1', 'A3', 'A4', 'A5']
	},
	{
		name: 'Megadede',
		models: ['Fiesta', 'Focus', 'Fusion', 'Mustang']
	}
]
const serviceGarages = [
	'A++ Auto Services',
	"Gary's Garage",
	'Super Service',
	'iGarage',
	'Best Service'
]

// Get Data Models
const Film = require('../models/Films');
// const Owner = require('../models/Owner')
// const Service = require('../models/Service')

// Fake data generation functions
// const generateOwnerData = () => {
// 	let ownerData = []
// 	let i = 0

// 	while (i < 50) {
// 		const firstName = faker.fake('{{name.firstName}}')
// 		const lastName = faker.fake('{{name.lastName}}')
// 		const email = faker.fake(`${firstName.toLowerCase()}.${lastName.toLowerCase()}@gmail.com`)

// 		const owner = {
// 			firstName,
// 			lastName,
// 			email
// 		}

// 		ownerData.push(owner)
// 		i++
// 	}

// 	return ownerData
// }

const generateFilmData = () => {
	let FilmData = []
	let i = 0

	while (i < 1000) {
		const slug = faker.random.arrayElement()
		const title = faker.random.arrayElement(Films.name)
		const money = faker.random.arrayElement(title.models)
		const year = faker.random.number({ min: 5000, max: 30000 })
		const director = faker.random.number(Films)

		const film = {
			slug,
			title,
			money,
			year,
			director
		}

		FilmData.push(film);
		i++
	}
return FilmData
}
// generateFilmData();

// const generateServiceData = carsIds => {
// 	let serviceData = []
// 	let i = 0

// 	while (i < 5000) {
// 		const car_id = faker.random.arrayElement(carsIds)
// 		const name = faker.random.arrayElement(serviceGarages)
// 		const date = faker.fake('{{date.past}}')

// 		const service = {
// 			car_id,
// 			name,
// 			date
// 		}

// 		serviceData.push(service)
// 		i++
// 	}

// 	return serviceData
// }

fastify.ready().then(
	async () => {
		try {
			console.log("entra try");
			// const owners = await Owner.insertMany(generateOwnerData())
			// const ownersIds = owners.map(x => x._id)
			var test  = generateFilmData();
			 const films = await Film.insertMany(test);
			 console.log(films);
			

			// const carsIds = films.map(x => x._id)

			// const services = await Service.insertMany(generateServiceData(carsIds))

			console.log(`
      Data successfully added:
   
        - ${films.length} films added.
      `)
		} catch (err) {
			throw boom.boomify(err)
		}
		process.exit()
	},
	err => {
		console.log(err);
		process.exit()
	}
)
