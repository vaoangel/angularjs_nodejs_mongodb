const { prisma } = require('../src/generated/prisma-client')

async function main() {
  await prisma.createUser({
    email: 'alice@prisma.io',
    name: 'Alice',
    password: '$2b$10$dqyYw5XovLjpmkYNiRDEWuwKaRAvLaG45fnXE5b3KTccKZcRPka2m', // "secret42"
    films: {
      create: {
        title: 'Titanic2',
        slug: 'Titanic2',
        published: true,
      },
    },
  })
  await prisma.createUser({
    email: 'bob@prisma.io',
    name: 'Bob',
    password: '$2b$10$o6KioO.taArzboM44Ig85O3ZFZYZpR3XD7mI8T29eP4znU/.xyJbW', // "secret43"
    films: {
      create: [
        {
          title: 'Titanic',
          slug: 'Titanic',
          published: true,
        },
        
      ],
    },
  })
}

main()
