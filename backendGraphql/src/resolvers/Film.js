const Film = {
    author: ({ id }, args, context) => {
      return context.prisma.film({ id }).author()
    },
 
  }
  

  module.exports = {
    Film,
  }