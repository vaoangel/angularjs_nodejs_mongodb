
const Mutation = {
  createFilm (parent, args, context){
    userId = getUserId(context)
    
    console.log(args)
    return context.prisma.createFilm({
      title: args.title,
      slug: args.slug,
      money: args.money,
      director: args.director,
      published: args.published,
      author: { connect: { id: userId } },
    })
  },
   deleteFilm  (parent , { id }, context) {
      return context.prisma.deleteFilm({ id })
  },
   publish (parent, { id }, context)  {
    return context.prisma.updateFilm({
      where: { id },
      data: { published: true },
    })
  }
}


module.exports = {
  Mutation
}