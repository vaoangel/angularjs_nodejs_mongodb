const Query = {
    film(parent, { id }, context) {
    return context.prisma.film({ id })
  },
  
    films(parent, args, context) {
    return context.prisma.films({})
  }
}

module.exports = {
Query
}
