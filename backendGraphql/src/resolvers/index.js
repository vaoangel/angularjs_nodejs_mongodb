const { Query } = require('./Query')
const { Mutation } = require('./Mutation')
const { User } = require('./User')
const { Film } = require('./Film')

const resolvers = {
  Query,
  Mutation,
  User,
 Film
}

module.exports = {
  resolvers,
}
