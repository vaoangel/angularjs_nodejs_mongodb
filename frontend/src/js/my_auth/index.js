import angular from 'angular';

let my_authModule = angular.module('app.my_auth', []);


import My_AuthConfig from './my_auth.config';
my_authModule.config(My_AuthConfig);

import My_AuthCtrl from './my_auth.controller';
my_authModule.controller('My_AuthCtrl',My_AuthCtrl);


export default my_authModule;