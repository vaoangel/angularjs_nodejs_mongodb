function My_AuthConfig($stateProvider){
    'ngInject';

    $stateProvider
    .state('app.my_register', {
        url: '/my_register',
        controller: 'My_AuthCtrl',
        controllerAs: '$ctrl',
        templateUrl:'my_auth/my_auth.html',
        title: 'Register'

    })

    .state('app.my_login', {
        url: '/my_login', 
        controller: 'My_AuthCtrl',
        controllerAs: '$ctrl',
        templateUrl:'my_auth/my_login.html',
        title: 'Login'
    })
}

export default My_AuthConfig;