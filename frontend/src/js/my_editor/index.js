import angular from 'angular';

let my_editorModule = angular.module('app.my_editor', []);

import EditorConfig from './editor.config';
my_editorModule.config(EditorConfig);

import EditorCtrl from './editor.controller';
my_editorModule.controller('EditorCtrl',EditorCtrl);

export default my_editorModule