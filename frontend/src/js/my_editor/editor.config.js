function EditorConfig($stateProvider) {
    'ngInject';
  
    $stateProvider
    .state('app.my_editor', {
      url: '/editor',
      controller: 'EditorCtrl',
      controllerAs: '$ctrl',
      templateUrl: 'my_editor/editor.html',
      title: 'Editor',
    })
    
  
  };
  
  export default EditorConfig;