function SettingsConfig($stateProvider){
    'ngInject';

  $stateProvider
  .state('app.my_settings', {
    url: '/my_settings',
    controller: 'my_SettingsCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'my_settings/settings.html',
    title: 'Settings',
    resolve: {
      auth: function(User) {
        return User.ensureAuthIs(true);
      }
    }
  });
}

export default SettingsConfig;
