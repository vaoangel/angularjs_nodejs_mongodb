import angular from 'angular';

let my_settingsModule = angular.module('app.my_settings', []);

import my_settingsConfig from './settings.config';
my_settingsModule.config(my_settingsConfig);

import my_SettingsCtrl from './settings.controller';
my_settingsModule.controller('my_SettingsCtrl', my_SettingsCtrl);

export default my_settingsModule;
