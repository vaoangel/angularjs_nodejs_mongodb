function FilmConfig($stateProvider){
    'ngInject'

    $stateProvider
    .state('app.film', {
      url: '/film',
      controller: 'FilmCtrl',
      controllerAs: '$ctrl',
      templateUrl: 'film/film.html',
      title: 'Film',
      resolve: {
        film: function(Film, $state, $stateParams) {
          return Film.get($stateParams.slug).then(
            (film) => film,
            (err) => $state.go('app.home')
          )
        }
      }
    });
  
  };
  export default FilmConfig;
  