import angular from 'angular';


let filmModule = angular.module('app.film', []);

import FilmConfig from './film.config';
filmModule.config(FilmConfig);


//Controllers

import FilmCrtl from './film.controller'
filmModule.controller('FilmCtrl', FilmCrtl);

export default filmModule;