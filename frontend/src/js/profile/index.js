import angular from 'angular';

let profileModule = angular.module('app.profile', []);
import profileConfig from './profile.config';
profileModule.config(profileConfig);

import ProfileCtrl from './profile.controller';
profileModule.controller('ProfileCtrl', ProfileCtrl);



export default profileModule;
