function ProfileConfig($stateProvider){
'ngInject';

$stateProvider
.state('app.profile',{
    url: '/profile/@:username/:author',
    controller: 'ProfileCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'profile/profile.html',

})
.state('app.profile.myFilm',{
    url:'/list/my_films',
    controller: 'ProfileCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'profile/myFilms.html',

})

.state('app.profile.authors',{
    url:'/list/authors',
    controller: 'ProfileCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'profile/myAuthors.html',

})


.state('app.profile.favorite',{
    url: '/profile/favorite/:filmid/:filmslug',
    controller: 'ProfileCtrl',
    controllerAs: '$ctrl',
    templateUrl:'profile/favorites.html',
   
})

.state('app.profile.films',{
    url:'/list/:username',
    controller: 'ProfileCtrl',
    controllerAs: '$ctrl',
    templateUrl: 'profile/list-fav-films.html',
})


}



export default ProfileConfig;