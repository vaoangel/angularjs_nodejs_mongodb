const AppConstants = {
  //api: 'https://conduit.productionready.io/api',
   api: 'http://localhost:3050/api',
   apiGrapql: 'http://localhost:3051/apiGrapql',
   moleculer : 'http://localhost:3060/api',
  jwtKey: 'jwtToken',
  appName: 'Coconut',
};

export default AppConstants;
