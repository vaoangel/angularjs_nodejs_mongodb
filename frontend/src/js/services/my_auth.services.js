export default class My_Auth {
    constructor(JWT,AppConstants, $http, $state, $stateParams,$q){
        'ngInject';

        this._AppConstants = AppConstants;
        this._$http = $http;
        this._$state = $state;
        this._$stateParams = $stateParams;
        this._$q = $q;
        this.current = null;

    }

    SendRegister(FormData){
        
        
            return this._$http({
              url: this._AppConstants.api + '/register',
              method: 'POST',
              data: {
                dataForm: FormData
              }
            }).then(
              (res) => {
        
                // this._JWT.save(res.data.user.token);
        
                return res;
              }
            );
          
    }

    SendLogin(FormData){
      return this._$http({
        url: this._AppConstants.api + '/login',
        method: 'POST',
        data : {
          dataForm: FormData
        }
      }).then(
        (res)=>{
          this.current = res.data.My_Auth;
          return res;
        }
      )
    }
}

