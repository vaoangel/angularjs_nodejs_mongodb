export default class User {
  constructor(JWT, AppConstants, $http, $state, $q) {
    'ngInject';

    this._JWT = JWT;
    this._AppConstants = AppConstants;
    this._$http = $http;
    this._$state = $state;
    this._$q = $q;

    this.current = null;

  }
  attemptAuth(FormData){
    // console.log(FormData)
    return this._$http({
      url: this._AppConstants.api + '/login',
      method: 'POST',
      data : {
        dataForm: FormData
      }
    }).then(
      (res)=>{
        this._JWT.save(res.data.my_auth.token);
        this.current = res.data.my_auth;
        return res;
      }
    )
  }


  // attemptAuth(type, credentials) {
  //   let route = (type === 'login') ? '/login' : '';
  //   return this._$http({
  //     url: this._AppConstants.api  + route,
  //     method: 'POST',
  //     data: {
  //       user: credentials
  //     }
  //   }).then(
  //     (res) => {
  //       this._JWT.save(res.data.user.token);
  //       this.current = res.data.user;

  //       return res;
  //     }
  //   );
  // }
  
  favorite(data){
    return this._$http({
      url:  this._AppConstants.api + '/favorite',
      method: 'PUT',
      data: { user: data.username, film: data.filmid }
    }).then(
      (res) => {
        console.log(res);
        return res;
      }
    )  }

    findFav(data){
      console.log(data)
      return this._$http({
        url:  this._AppConstants.api + '/findFav',
        method: 'PUT',
        data: { user: data }
      }).then(
        (res) => {
          // console.log("entrauserservice")
          console.log(res);
          return res;
        }
      )  
    }
    
  update(fields) {
    return this._$http({
      url:  this._AppConstants.api + '/user',
      method: 'PUT',
      data: { user: fields }
    }).then(
      (res) => {
        this.current = res.data.user;
        return res.data.user;
      }
    )
  }

  logout() {
    this.current = null;
    this._JWT.destroy();
    this._$state.go(this._$state.$current, null, { reload: true });
  }

  verifyAuth() {
    let deferred = this._$q.defer();

    // check for JWT token
    if (!this._JWT.get()) {
      deferred.resolve(false);
      return deferred.promise;
    }

    if (this.current) {
      deferred.resolve(true);

    } else {
      this._$http({
        url: this._AppConstants.api + '/user',
        method: 'GET',
        headers: {
          Authorization: 'Token ' + this._JWT.get()
        }
      }).then(
        (res) => {
          this.current = res.data.user;
          deferred.resolve(true);
        },

        (err) => {
          this._JWT.destroy();
          deferred.resolve(false);
        }
      )
    }

    return deferred.promise;
  }


  ensureAuthIs(bool) {
    let deferred = this._$q.defer();

    this.verifyAuth().then((authValid) => {
      // console.log(authValid);
      if (authValid !== bool) {
        this._$state.go('app.home')
        deferred.resolve(false);
      } else {
        deferred.resolve(true);
      }

    });

    return deferred.promise;
  }

 
findAuthor(User){
  // console.log(User)
  return this._$http({
    url:  this._AppConstants.api + '/author',
    method: 'POST',
    data: { user: User }
  }).then(
    (res) => {
    
      return res.data;
    }
  )
}

detailsAuthor(id){
  console.log(id)
  return this._$http({
    url:  this._AppConstants.api + '/findAuthorById',
    method: 'POST',
    data: { author: id }
  }).then(
    (res) => {
    
      return res.data;
    }
  )
}
}
