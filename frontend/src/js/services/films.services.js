export default class Films {
    constructor(AppConstants, $http, $q){
        'ngInject';
        this._AppConstants = AppConstants;
        this._$http = $http;
        this._$q = $q;
    }


    getAll() {

        return this._$http({
            url: `${this._AppConstants.api}/films`,
            method: 'GET',
        }).then((res) => res.data);
    
    }

  getAllGraphql(){
    return this._$http({
      url: `${this._AppConstants.api}/graphql/graphql?query={films{slug}}`,
      method : 'GET'

    }).then((res) => console.log(res));
  }
  getDetails(id) {

    return this._$http({
        url: `${this._AppConstants.api}/getById/`+id,
        method: 'GET',
    }).then((res) => res.data);

}

addFilm(data) {
  return this._$http({
    url: this._AppConstants.api + '/addFilm',
    method: 'POST',
    data : {
      dataForm: data
    }
  }).then(
    (res) => {
      console.log(res);
      return res;
    });
}

myFilms(data){
  return this._$http({
    url: this._AppConstants.api + '/myFilms',
    method: 'POST',
    data : {
      dataForm: data
    }
  }).then(
    (res) => {
      console.log(res);
      return res;
    });
}
}
