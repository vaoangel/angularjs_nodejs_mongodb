export default class Profile {
  constructor (AppConstants, $http) {
    'ngInject';

    this._AppConstants = AppConstants;
    this._$http = $http;

  }

  get(username) {
    return this._$http({
      url: this._AppConstants.api + '/profiles/' + username,
      method: 'GET'
    }).then((res) => res.data.profile);
  }

  follow(data) {
    return this._$http({
      url: this._AppConstants.api + '/profiles/follow',
      method: 'POST',
      data: { users: data }
    }).then((res) => res.data);
  }

  unfollow(data) {
    return this._$http({
      url: this._AppConstants.api + '/profiles/unfollow',
      method: 'POST',
      data: { users: data }
    }).then((res) => res.data);
  }

}
