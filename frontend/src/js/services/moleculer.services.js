export default class Moleculer {
    constructor(JWT,AppConstants, $http, $state, $stateParams,$q){
        'ngInject';

        this._AppConstants = AppConstants;
        this._$http = $http;
        this._$state = $state;
        this._$stateParams = $stateParams;
        this._$q = $q;
        this.current = null;

    }


    findFilms(){
        return this._$http({
            url: this._AppConstants.moleculer + '/films',
            method: 'GET',
        
          }).then(
            (res) => {
              console.log(res)
              // this._JWT.save(res.data.user.token);
      
              return res;
            }
          );
    }
}