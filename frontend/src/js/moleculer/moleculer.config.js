function MoleculerConfig($stateProvider) {
    'ngInject';
  
    $stateProvider
    .state('app.moleculer', {
      url: '/moleculer',
      controller: 'MolCtrl',
      controllerAs: '$ctrl',
      templateUrl: 'moleculer/moleculer.html',
      title: 'Moleculer'
    });
  
  };
  
  export default MoleculerConfig;