import angular from 'angular';


let moleculerModule = angular.module('app.moleculer', []);


import MoleculerConfig from './moleculer.config';
moleculerModule.config(MoleculerConfig);

import MoleculerCtrl from './moleculer.controller';
moleculerModule.controller('MolCtrl', MoleculerCtrl);

export default moleculerModule;