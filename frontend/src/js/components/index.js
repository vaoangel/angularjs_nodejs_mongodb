import angular from 'angular';

let componentsModule = angular.module('app.components', []);


import ListErrors from './list-errors.component'
componentsModule.component('listErrors', ListErrors);

import ShowAuthed from './show-authed.directive';
componentsModule.directive('showAuthed', ShowAuthed);

import FollowBtn from './buttons/follow-btn.component';
componentsModule.component('followBtn', FollowBtn);

import ArticleMeta from './article-helpers/article-meta.component';
componentsModule.component('articleMeta', ArticleMeta);

import FavoriteBtn from './buttons/favorite-btn.component';
componentsModule.component('favoriteBtn', FavoriteBtn);

import ArticlePreview from './article-helpers/article-preview.component';
componentsModule.component('articlePreview', ArticlePreview);

import ArticleList from './article-helpers/article-list.component';
componentsModule.component('articleList', ArticleList);

import ListPagination from './article-helpers/list-pagination.component';
componentsModule.component('listPagination', ListPagination);

 import FilmList from './film-components/list-film.component';
 componentsModule.component('filmList', FilmList);

 import Details from './film-components/global-details.component';
 componentsModule.component('details',Details);

 import Register from './auth_components/register.component';
 componentsModule.component('register', Register);

 import Login from './auth_components/login.component';
 componentsModule.component('login', Login);

 import Profile from './profile-components/main-profile.component';
 componentsModule.component('profile', Profile);

 import Settings from './profile-components/update-settings.component';
 componentsModule.component('settings', Settings);

 import Favorites  from './profile-components/favorites.component';
 componentsModule.component('favorite', Favorites);

 import ListFav from './profile-components/list-fav-films.component';
 componentsModule.component('list', ListFav);

 import addEditor from './editor-components/addEditor.component';
 componentsModule.component('neweditor', addEditor);

 import myFilms from './profile-components/myFilms.component';
 componentsModule.component('myfilms', myFilms);

 import authors from './profile-components/authors.component';
 componentsModule.component('authors', authors);

 import moleculer from './moleculer-components/moleculer.component';
 componentsModule.component('moleculer', moleculer);
export default componentsModule;
