class ListFavCtrl{
    constructor(User,$scope,Films){
        'ngInject';
        this._User = User;
        this._$scope = $scope;
        this._Films = Films;
        this.formData = {
            email: User.current.email,
            bio: User.current.bio,
            image: User.current.image,
            username: User.current.username
          }
        this.findFav();
        console.log("favCtrl")
    }

    findFav(){
        var arrayFilms = new Array();
        this._User.findFav(this.formData).then(
            (response) => {
              console.log(response)
                // console.log(response.data.__v)
                for(var i = 0; i< response.data.favorites.length; i++){
                    this._Films 
                    .getDetails(response.data.favorites[i])
                      .then((films) => {
                        // console.log(films)
                      arrayFilms.push(films);
                      }
                      )
                }

                console.log(arrayFilms);
                this._$scope.films = arrayFilms;
            }
        )
    }
}
 let ListFav = {
    bindings: {
        limit: '=',
        listConfig: '='
      },
      controller: ListFavCtrl,
      templateUrl: 'components/profile-components/list-fav-films.html'
    };
    
export default ListFav;