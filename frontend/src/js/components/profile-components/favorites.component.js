class FavCtrl{
    constructor(User,$scope,$stateParams){
        'ngInject';
        this._User = User;
        this.$scope = $scope;
        this.$stateParams = $stateParams;

       
        this.data = {
            username :User.current.username,
            filmid :$stateParams.filmid
        }
        this.addFav();
        // this.findFav();
    }

    addFav(){

         this._User.favorite(this.data).then(
             (user) => {
                 console.log(user);
             },
             (err) => {
                this.isSubmitting = false;
                this.errors = err.data.errors;
             }
         )
    }

  
}


let Favs = {
    controller: FavCtrl,
    templateUrl: 'components/profile-components/fav-component.html'

}

export default Favs;