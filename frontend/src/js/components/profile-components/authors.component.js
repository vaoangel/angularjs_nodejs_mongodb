class AuthorsCtrl{
    constructor(User, $state, $scope){
        this._User = User;
        this._$state = $state;
        this._$scope = $scope;
        this.formData = {
            email: User.current.email,
            bio: User.current.bio,
            image: User.current.image,
            username: User.current.username
          }
        this.runQuery();
        
    }

    runQuery(){
// console.log(this.formData)
        var arrayAuthors = new Array();
        this._User.findFav(this.formData).then(
            (response) => {
            //     console.log(response);
            //    console.log(response.data.following)
                for(var i = 0; i< response.data.following.length; i++){
                    this._User 
                    .detailsAuthor(response.data.following[i])
                      .then((authors) => {
                        // console.log(films)
                        arrayAuthors.push(authors);
                      }
                      )
                }

                //  console.log(arrayAuthors);
                 this._$scope.authors = arrayAuthors;
            }
        )
    }
}


let Authors = {
    controller : AuthorsCtrl,
    templateUrl : 'components/profile-components/authors.component.html'
}

export default Authors;