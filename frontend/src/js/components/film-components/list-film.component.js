console.log("entra");
class FilmListCtrl {
    constructor(Films, $scope, User){
      // console.log(Films)
        'njInject';
        this._Films = Films;
        this._$scope = $scope;
        this._User = User;
        this.setListTo(this.listConfig);
console.log(this._User.current);

        $scope.$on('setListTo', (ev, newList) => {
          this.setListTo(newList);
        });
    
        $scope.$on('setPageTo', (ev, pageNumber) => {
          this.setPageTo(pageNumber);
        });
    }
    setListTo(newList) {
      // Set the current list to an empty array
      this.list = [];
  
      // Set listConfig to the new list's config
      this.listConfig = newList;
  
      this.runQuery();
    }
  
    setPageTo(pageNumber) {
      this.listConfig.currentPage = pageNumber;
  
     
    }
runQuery(){
//  console.log(
//    "runquery films"
//  );
 this._Films 
 .getAll()
   .then((films) => {
    //  console.log(films)
      
     this.films = films;
     this._$scope.films = films;

     
   }
   )

  //  this._Films.getAllGraphql().then(
  //    (response) => {
  //      console.log(response)
  //    }
  //  )
}

}

let filmList = {
    bindings: {
      limit: '=',
      listConfig: '='
    },
    controller: FilmListCtrl,
    templateUrl: 'components/film-components/film-list.html'
  };
  
  export default filmList;

