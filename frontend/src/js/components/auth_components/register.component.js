class RegisterCtrl{
    constructor($scope,My_Auth,$state){
        'njInject';
        this._$scope = $scope;
        this._My_Auth = My_Auth;
        this._$state = $state;
        this.runQuery();
    }

    runQuery(){
        // console.log("entra runquery register");

        
    }


    submitForm() {
        this.isSubmitting = true;
    //console.log(this.formData);

    // var pp = this._My_Auth.SendRegister(this.formData);
   this._My_Auth.SendRegister(this.formData).then(
       (res) => {
        this._$state.go('app.home');
           }, 
           (err) => {
               this.isSubmitting = false;
               this.errors = err.data.errors;
           }
   )
     
      }
    
}

let Register ={
    controller: RegisterCtrl, 
    templateUrl : 'components/auth_components/register.html'
}
export default Register;
