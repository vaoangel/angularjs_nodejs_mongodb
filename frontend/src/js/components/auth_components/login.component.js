class LoginCtrl{
    constructor($scope,My_Auth,$state){
        'njInject';
        this._$scope = $scope;
        this._My_Auth = My_Auth;
        this._$state = $state;
        this.current = null;
    }
    submitForm() {
        this.isSubmitting = true;
    // console.log(this.formData);

    // var pp = this._My_Auth.SendRegister(this.formData);
   this._My_Auth.SendLogin(this.formData).then(
       (res) => {
        //    console.log(res)
// console.log(this._My_Auth._currentAuth)
              this._$state.go('app.home');

           }, 
           (err) => {
               this.isSubmitting = false;
               this.errors = err.data.errors;
           }
   )
     
      }
}

let Login ={
    controller: LoginCtrl, 
    templateUrl : 'components/auth_components/login.html'
}
export default Login;