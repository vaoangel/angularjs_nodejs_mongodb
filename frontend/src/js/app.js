import angular from 'angular';

// Import our app config files
import constants  from './config/app.constants';
import appConfig  from './config/app.config';
import appRun     from './config/app.run';
import 'angular-ui-bootstrap';
import 'angular-animate';
import 'angular-ui-router';
import toastr from 'angular-toastr';
import 'angular-messages';
import 'angular-stripe';
import 'ng-file-upload';
import satellizer from '../../bower_components/satellizer';




// Import our templates file (generated by Gulp)
import './config/app.templates';
// Import our app functionaity
import './layout';
import './contact';
import './components';
import './home';
import './profile';
import './services';
import './auth';
import './my_settings';
import './my_editor'
import './film';
import './globalDetails';
import './my_auth';
import './moleculer'
//Mine funcionality



// Create and bootstrap application
const requires = [
  'ui.router',
  'templates',
  'app.layout',
  'app.contact',
  'app.components',
  'app.home',
  'app.my_editor',
  'app.profile',
  'app.services',
  'app.auth',
  'app.my_settings',
  'app.film',
  'app.details',
  'app.my_auth',
  'app.moleculer',
  satellizer,
  toastr
];

// Mount on window for testing
window.app = angular.module('app', requires);

angular.module('app').constant('AppConstants', constants);

angular.module('app').config(appConfig);

angular.module('app').run(appRun);

angular.bootstrap(document, ['app'], {
  // strictDi: true
});
