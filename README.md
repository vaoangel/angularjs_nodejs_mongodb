# !NodeJs, AngularJs[1.5], Bakend Prisma and Graphql, Backend with Moleculer, Client Running with gulp




> ### App with one frontend and three different backend, all the backends are with docker




# Getting started

To get the Node server running locally:

- Clone this repo
- `sudo docker-compose up --build `to run all the mongo backend and the moleculer backend
- `cd frontend , gulp ` to run the frontend


# Code Overview

## Dependencies
- `start up portainer :  sudo docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer`

- `fastify` The server for handling and routing HTTP requests
- `fastify-jwt`() - Middleware for validating JWTs for authentication
- [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken) - For generating JWTs used by authentication
- [mongoose](https://github.com/Automattic/mongoose) - For modeling and mapping MongoDB data to javascript 
- [mongoose-unique-validator](https://github.com/blakehaswell/mongoose-unique-validator) - For handling unique validation errors in Mongoose. Mongoose only handles validation at the document level, so a unique index across a collection will throw an exception at the driver level. The `mongoose-unique-validator` plugin helps us by formatting the error like a normal mongoose `ValidationError`.
- `Moleculer service connected`
- `Prisma with graphql connected`
- [slug](https://github.com/dodo/node-slug) - For encoding titles into a URL-friendly format

- `All the pages on the frontend are maded on components of angularJs 1.5`

- `The graphql service will be running on this url http://localhost:3050/graphiql.html , once you have started the server` 

## Application Structure

- `app.js` - The entry point to our application. This file defines our fastify server and connects it to MongoDB using mongoose. It also requires the routes and models we'll be using in the application.
- `config/` - This folder contains configuration for passport as well as a central location for configuration/environment variables.
- `Controllers/` Contains the controllers of the modules
- `routes/` - This folder contains the route definitions for our API.
- `models/` - This folder contains the schema definitions for our Mongoose models.



## Authentication

Requests are maded to the backend and jsonwebtoken secures if the user is authenticated or not, if the user is authenticated he will be able to do any critical operation


## Author

`Àngel Vañó Francés`
